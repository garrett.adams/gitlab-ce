# frozen_string_literal: true

class OauthExpireWorker
  include ApplicationWorker
  include CronjobQueue

  queue_namespace :cronjob

  def perform
    Rails.logger.info "#{self.class}: Checking for expired Oauth tokens"
    User.find_each do |user|
      user.identities.each do |identity|
        expire_time = identity.expires_at.to_i
        current_time = Time.now.to_i

        if current_time > expire_time
          identity.token = ""
          identity.save
        end
      end
    end
  end
end
